# Temperature Conversion

Temperature conversion from volts to celsius will be made using linear 
function. The slope and offset was calculated using the document 
PowerAmplifiersCharacteristic.pdf . The values were calculated using
the polyfit funtion from numpy.
Here the list of the values for each amplifier

## 352 SN1

* Slope: 110.48940027
* Offset: -201.48297738

## 704 SN1

* Slope: 111.10447048
* Offset: -203.4855367

## 352 SN2

* Slope: 111.10447048
* Offset: -204.5965814 

## 704 SN2
* Slope:  109.71153099
* Offset: -199.13517611
