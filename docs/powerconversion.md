# Power Conversion

Power conversion from volts to dBm should be done using 3rd polynomial 
approximation. Using the PowerAmplifiersCharacteristic.pdf values and 
the numpy.polyfit function was found the following polynomial 
coefficients

## Amplifier 352 SN 1

* Input Power: 2.46226343, -16.32656351, 57.26007182, -81.37492672
* Output Power:  -2.95436077, 19.87345068, -23.0033706, 23.41175038


## Amplifier 704 SN 1

* Input Power: 2.4902771 , -15.87393181,  54.97965152, -73.55905381
* Output Power: -1.96875378, 12.54978986, -4.30032447,  6.36018946

## Amplifier 352 SN 2

* Input Power:  0.2640623 ,  -1.92988227,  26.02300275, -57.99129332
* Output Power: -1.4767723 , 10.73490398, -4.25970565,  7.03488562

## Amplifier 704 SN 2

* Input Power: 2.89460263, -18.45904057,  59.93777013, -76.5436577 
* Output Power:  0.07906806,  -1.1868651 ,  25.16604828, -13.56959454
