# This should be a test startup script
require xtpico, develop
require prl_amplifier

epicsEnvSet("P",           "TEST:")
epicsEnvSet("DEVICE_IP",        "127.0.0.1")

epicsEnvSet("SPI_COMM_PORT1",    "AK_SPI_COMM1")

drvAsynIPPortConfigure("$(SPI_COMM_PORT1)","$(DEVICE_IP):9999")

AKSPIPRLHPAConfigure("PRLHPA.1", "$(SPI_COMM_PORT1)", 0, 0)
dbLoadRecords("hp352sn1.db", "P=$(P), R=, PORT=PRLHPA.1, IP_PORT=$(SPI_COMM_PORT1),  ADDR=0, TIMEOUT=1")

#iocshLoad("$(prl_amplifier_DIR)/prl_amplifier.iocsh")
