# Template for PRL amplifier

record(ai,"$(P)$(R)InPwrRaw" ){
    field(DESC, "Input power from XT Nano")
    field(EGU, "Volts")
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))AKSPI_PRLHPA_INPWR")
}

record(ai,"$(P)$(R)InPwr" ){
    field(DESC, "Input power converted to dBm")
    field(EGU, "dBm")
    field(HIGH, "$(HIGHINPWR)")
    field(LOW, "$(LOWINPWR)")
}

record(ai,"$(P)$(R)OutPwrRaw" ){
    field(DESC, "Output power from XT Nano")
    field(EGU, "Volts")
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))AKSPI_PRLHPA_OUTPWR")
}

record(ai,"$(P)$(R)OutPwr" ){
    field(DESC, "Output power converted to dBm")
    field(EGU, "dBm")
    field(HIGH, "$(HIGHOUTPWR)")
    field(LOW, "$(LOWOUTPWR)")
}

record(ai,"$(P)$(R)RefPwrRaw" ){
    field(DESC, "Reflected power from XT Nano")
    field(EGU, "Volts")
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))AKSPI_PRLHPA_REFPWR")
}

record(ai,"$(P)$(R)RefPwr" ){
    field(DESC, "Reflected power converted to dBm")
    field(EGU, "dBm")
    field(HIGH, "$(HIGHREFPWR)")
    field(LOW, "$(LOWREFPWR)")
}

record(ai, "$(P)$(R)Temp") {
    field(DESC, "Temperature")
    field(ASLO, "$(TEMPASLO)")
    field(AOFF, "$(TEMPAOFF)")
}


record(bo, "$(P)$(R)OnOff")
{
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))AKSPI_PRLHPA_ONOFF")
    field(ONAM, "On")
    field(ZNAM, "Off")
    field(SCAN, "Passive")
}

record(bi, "$(P)$(R)OnOff-RB")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))AKSPI_PRLHPA_ONOFF")
    field(ONAM, "On")
    field(ZNAM, "Off")
    field(SCAN, "I/O Intr")
}
